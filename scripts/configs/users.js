'use strict';
angular.module('pointofsale')
    .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('main.useraccounts', {
                url: '/useraccounts',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/useraccounts/useraccounts.html',
                        controller: 'useraccountsCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'User Accounts and Privileges',
                    parent: 'main.home'
                },
                data: {
                    module: 'User Accounts and Privileges',
                    tab: 12
                }
            })
            .state('main.permissions', {
                url: '/permissions',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/useraccounts/permission.html',
                        controller: 'permissionCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'User Group',
                    parent: 'main.useraccounts'
                },
                data: {
                    module: 'User Group',
                    tab: 12
                }
            })
            .state('main.rolepermissions', {
                url: '/permissions/:roleID',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/settings/useraccounts/permission.html',
                        controller: 'permissionCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'User Group',
                    parent: 'main.useraccounts'
                },
                data: {
                    module: 'User Group',
                    tab: 12
                }
            });
    });
