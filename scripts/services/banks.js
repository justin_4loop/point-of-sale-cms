'use strict';
angular.module('pointofsale')
    .factory('banksFctry', function($http, API_URL) {
        return {

            getAllBanks: function() {
                return $http({
                    url: API_URL + '/banks',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                  
                });
            },
            getBank: function(id) {
                return $http({
                    url: API_URL + '/banks/' + id,
                    method: 'GET',
                }).then(function(res) {
                     return res.data;
                });
            },
            deleteBank: function(id) {
                return $http({
                    url: API_URL + '/banks/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });

            },
            saveBank: function(data) {
                return $http({
                    url: API_URL + '/banks',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateBank: function(id, data) {
                return $http({
                    url: API_URL + '/banks/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
