'use strict';

angular.module('pointofsale')
    .factory('variationFactory', function($http, API_URL) {
        return {
            getAllVariation: function() {
                return $http({
                    url: API_URL + '/variation',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getVariation: function(id) {
                return $http({
                    url: API_URL + '/variation/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteVariation: function(id) {
                return $http({
                    url: API_URL + '/variation/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveVariation: function(data) {
                return $http({
                    url: API_URL + '/variation',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateVariation: function(id, data) {
                return $http({
                    url: API_URL + '/variation/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
