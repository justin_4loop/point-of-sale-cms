'use strict';

angular.module('pointofsale')
    .factory('employeeFctry', function($http, API_URL) {
        return {
            getAllEmployees: function() {
                return $http({
                    url: API_URL + '/employees',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            saveEmployee: function(data) {
                console.log(data);
                return $http({
                    url: API_URL + '/employees',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });
            },
            updateEmployee: function(id,data) {
                console.log(data);
                return $http({
                    url: API_URL + '/employees/' + id,
                    data: data,
                    method: 'PUT',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteEmployee: function(id) {
                return $http({
                    url: API_URL + '/employees/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    return res.data;
                });
            },
            getEmployee: function(id) {
                return $http({
                    url: API_URL + '/employees/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
