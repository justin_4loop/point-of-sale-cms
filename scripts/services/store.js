'use strict';

angular.module('pointofsale')
    .factory('storeFactory', function($http, API_URL) {
        return {
            getAllStore: function() {
                return $http({
                    url: API_URL + '/store',
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            getStore: function(id) {
                return $http({
                    url: API_URL + '/store/' + id,
                    method: 'GET',
                }).then(function(res) {
                    return res.data;
                });
            },
            deleteStore: function(id,cb) {
                return $http({
                    url: API_URL + '/store/' + id,
                    method: 'DELETE',
                }).then(function(res) {
                    cb(res.data);
                });

                
            },
            saveStore: function(data) {
                console.log(data);
                return $http({
                    url: API_URL + '/store',
                    data: data,
                    method: 'POST',
                }).then(function(res) {
                    return res.data;
                });

                
            },
            updateStore: function(id, data) {
                return $http({
                    url: API_URL + '/store/' + id,
                    method: 'PUT',
                    data: data
                }).then(function(res) {
                    return res.data;
                });
            }
        };
    });
