'use strict';

angular.module('pointofsale')
    .directive('appHeader', function() {
        return {
            restrict: 'AE',
            templateUrl: 'scripts/directives/header/header.html',
            replace: true,
            controller: 'headerCTRL'
        };
    });
  