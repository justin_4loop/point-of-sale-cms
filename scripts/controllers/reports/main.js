 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('mainReportCtrl', function($scope, $filter, $window, toastr) {
         $scope.lineChartslabels = ["January", "February", "March", "April", "May", "June", "July"];
         $scope.lineChartsseries = ['Series A', 'Series B'];
         $scope.lineChartdata = [
             [65, 59, 80, 81, 56, 55, 40],
             [28, 48, 40, 19, 86, 27, 90]
         ];

         $scope.donutlabels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
         $scope.donutdata = [300, 500, 100];
     });
