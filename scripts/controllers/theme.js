 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('ThemeController', function($scope, $window, _ ,$cookieStore) {
         $scope.themes = [{
             name: "Default",
             url: "bower_components/bootstrap/dist/css/bootstrap.min.css"
         }, {
             name: "Lumen",
             url: "styles/themes/lumen/bootstrap.min.css"
         }, {
             name: "Cerulean",
             url: "styles/themes/cerulean/bootstrap.min.css"
         }, {
             name: "Cosmo",
             url: "styles/themes/cosmo/bootstrap.min.css"
         }, {
             name: "Paper",
             url: "styles/themes/paper/bootstrap.min.css"
         }, {
             name: "Flatly",
             url: "styles/themes/flatly/bootstrap.min.css"
         }, {
             name: "United",
             url: "styles/themes/united/bootstrap.min.css"
         }];

         var theme = $cookieStore.get('theme');
         $scope.theme = !_.isUndefined(theme) ? JSON.parse($cookieStore.get('theme')) : $scope.themes[0];

         $scope.setTheme = function(theme) {
             if (theme.name !== $scope.theme.name) {
                 $scope.theme = theme;
                 $cookieStore.put('theme', JSON.stringify(theme));
             }
         };
     });
