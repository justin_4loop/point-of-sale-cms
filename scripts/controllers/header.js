 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')

     .controller('headerCTRL', function($rootScope, $scope, $state) {
         $scope.logout = function() {
             $state.go('auth.login');
         };

     });
