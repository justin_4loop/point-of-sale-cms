/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('supplierProductCtrl', function(_, $scope, $filter, $state, $rootScope, $window, $timeout, $stateParams, async, supplierFctry, productFactory, attributeFactory, ptypeFactory, toastr, ngDialog, ngTableParams) {

        $scope.suppliers = [];
        $scope.ptypes = [];
        $scope.attributes = [];

        $scope.activeTab = 'product';

        $scope.prod = {};
        $scope.tab = {
            product: true,
            store: false,
            price: false,
            category: false,
            attribute: false,
            variation: false
        };

        $scope.tabprod = {
            product: false,
            store: true,
            price: true,
            category: true,
            attribute: true,
            variation: true
        };

        $scope.minDate = new Date(1900, 1, 1);
        $scope.datepopup1 = {
            opened: false
        };
        $scope.datepopup2 = {
            opened: false
        };


        async.waterfall([
            function(callback) {
                supplierFctry.getAllSuppliers().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.suppliers = data.response.result;
                        callback(null, $scope.suppliers);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            },
            function(suppliers, callback) {
                ptypeFactory.getAllProdType().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.ptypes = data.response.result;
                        callback(null, $scope.ptypes, suppliers);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            },
            function(ptypes, suppliers, callback) {
                attributeFactory.getAllAttribute().then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        $scope.attributes = data.response.result;
                        callback(null, $scope.attributes, ptypes, suppliers);
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            },
            function(attributes, ptype, suppliers, callback) {
                if ($stateParams.product_id) {
                    productFactory.getProduct($stateParams.product_id).then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            if (data.response.result.length > 0) {

                                $scope.tabprod = {
                                    product: false,
                                    store: false,
                                    price: false,
                                    category: false,
                                    attribute: false,
                                    variation: false
                                };

                                $scope.prod = data.response.result[0];
                                $scope.prod.start_date = new Date($scope.prod.start_date);
                                $scope.prod.end_date = new Date($scope.prod.end_date);

                                $scope.prod.is_published = $scope.IntToBoolean($scope.prod.is_published);
                                $scope.prod.is_archived = $scope.IntToBoolean($scope.prod.is_archived);
                            }
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                } else {

                    $scope.tabprod = {
                        product: false,
                        store: true,
                        price: true,
                        category: true,
                        attribute: true,
                        variation: true
                    };

                    $scope.prod.is_published = false;
                    $scope.prod.is_archived = false;
                    $scope.prod.start_date = new Date();
                    $scope.prod.end_date = new Date();
                }
            }
        ]);

        $scope.openStartDate = function() {
            $scope.datepopup1.opened = true;
        };

        $scope.openEndDate = function() {
            $scope.datepopup2.opened = true;
        };


        $scope.gotoProduct = function() {
            $state.go('main.addproduct', {
                product_id: $stateParams.product_id
            });
        };

        $scope.gotoAssociation = function() {
            $state.go('main.association', {
                product_id: $stateParams.product_id
            });
        };

        $scope.IntToBoolean = function(value) {
            return (value === 1) ? true : false;
        };

        $scope.BooleanToInt = function(bool) {
            return (bool === true) ? 1 : 0;
        };

        $scope.tabclick = function(tab) {
            $scope.activeTab = tab;
            switch (tab) {
                case 'product':
                    $scope.tab.product = true;
                    $scope.tab.store = false;
                    $scope.tab.price = false;
                    $scope.tab.category = false;
                    $scope.tab.attribute = false;
                    $scope.tab.variation = false;
                    break;
                case 'store':
                    $scope.tab.product = false;
                    $scope.tab.store = true;
                    $scope.tab.price = false;
                    $scope.tab.category = false;
                    $scope.tab.attribute = false;
                    $scope.tab.variation = false;
                    break;
                case 'price':
                    $scope.tab.product = false;
                    $scope.tab.store = false;
                    $scope.tab.price = true;
                    $scope.tab.category = false;
                    $scope.tab.attribute = false;
                    $scope.tab.variation = false;
                    break;
                case 'category':
                    $scope.tab.product = false;
                    $scope.tab.store = false;
                    $scope.tab.price = false;
                    $scope.tab.category = true;
                    $scope.tab.attribute = false;
                    $scope.tab.variation = false;
                    break;
                case 'attribute':
                    $scope.tab.product = false;
                    $scope.tab.store = false;
                    $scope.tab.price = false;
                    $scope.tab.category = false;
                    $scope.tab.attribute = true;
                    $scope.tab.variation = false;
                    break;
                case 'variation':
                    $scope.tab.product = false;
                    $scope.tab.store = false;
                    $scope.tab.price = false;
                    $scope.tab.category = false;
                    $scope.tab.attribute = false;
                    $scope.tab.variation = true;
                    break;
            }
        };

        function saveProd() {
            if ($stateParams.product_id) {
                productFactory.updateProduct($stateParams.product_id, $scope.prod).then(function(data) {
                    console.log('updateProduct: ', data);
                    if (data.statusCode === 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $state.go($state.current, {}, { reload: true });

                        $scope.tabprod = {
                            product: false,
                            store: false,
                            price: false,
                            category: false,
                            attribute: false,
                            variation: false
                        };
                        /*$timeout(function() {
                            $state.go('main.viewProduct');
                        }, 300);*/
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            } else {
                productFactory.saveProduct($scope.prod).then(function(data) {
                    if (data.statusCode === 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.go('main.viewProduct');
                        }, 300);
                    } else if (data.statusCode === 200 && !data.success && _.isArray(data.response)) {
                        _.each(data.response, function(msg) {
                            toastr.warning(msg.msg, 'Warning');
                        });
                    } else {
                        toastr.error(data.response.msg, 'Error');
                    }
                });
            }
        }

        $scope.actionSave = function() {
            switch ($scope.activeTab) {
                case 'product':
                    saveProd();
                    break;
                case 'store':
                    $scope.$broadcast('saveStore');
                    break;
                case 'price':
                    $scope.$broadcast('savePrice');
                    break;
                case 'category':
                    $scope.$broadcast('saveCategory');
                    break;
                case 'attribute':
                    $scope.$broadcast('saveAttribute');
                    break;
                case 'variation':
                    $scope.$broadcast('saveVariation');
                    break;
            }
        };
    });
