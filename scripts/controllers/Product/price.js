 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('priceCTRL', function(_, $scope, $rootScope, $stateParams, $state, async, toastr, storeFactory, priceHistoryFactory) {

         $scope.ST = {};

         $scope.stores = [];
         $scope.storesCopy = [];

         $scope.itemArray = [];
         $scope.itemArrayCopy = [];


         async.waterfall([
             function(callback) {
                 storeFactory.getAllStore().then(function(data) {
                     if (data.statusCode == 200 && data.response.success) {
                         $scope.stores = data.response.result;
                         $scope.storesCopy = angular.copy($scope.stores);

                         callback(null, $scope.stores);
                     } else {
                         toastr.error(data.response.msg, 'Error');
                     }
                 });
             },
             function(stores, callback) {
                 if ($stateParams.product_id) {
                     priceHistoryFactory.getPriceHistory($stateParams.product_id).then(function(data) {
                         if (data.statusCode == 200 && data.response.success) {
                             $scope.itemArray = data.response.result;
                             $scope.itemArrayCopy = angular.copy($scope.itemArray);
                         } else {
                             toastr.error(data.response.msg, 'Error');
                         }
                     });
                 }
             }
         ]);


         $scope.addValue = function() {
             $scope.itemArray.push({
                 base_price: '0',
                 fk_product_id: $stateParams.product_id,
                 regular_price: '0',
                 rrp: '0',
                 sale_percent: '0',
                 sale_price: '0',
             });
         };


         $scope.remove = function(index) {
             $scope.itemArray.splice(index, 1);
         };

         function hasDuplicates(array) {
             return _.some(array, function(elt, index) {
                 return array.indexOf(elt) !== index;
             });
         }

         function savePH() {
             var arr = [];

             var result = _.map($scope.itemArray, function(n) {
                 return n.store_id;
             });

             if (hasDuplicates(result)) {
                 toastr.warning('WARNING: One of the selected store already existed', 'WARNING');
                 return;
             }


             priceHistoryFactory.saveSP($stateParams.product_id, $scope.itemArray).then(function(data) {
                 if (data.statusCode === 200 && data.response.success) {
                     toastr.success(data.response.msg, 'Success');
                     $state.go($state.current, {}, { reload: true });
                 } else {
                     toastr.error(data.response.msg, 'Error');
                 }
             });

         }

         $scope.$on('savePrice', function(e) {
             savePH();
         });

     });
