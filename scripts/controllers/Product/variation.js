 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('varCTRL', function($scope) {
         $scope.itemArray = [{
             id: 1,
             attrib1: 'Red',
             attrib2: 'S',
             discount: '100',
             SKU: '123456',
             inventory: '22'
         }, {
             id: 2,
             attrib1: 'Red',
             attrib2: 'M',
             discount: '200',
             SKU: '123w6',
             inventory: '23'
         }, {
             id: 3,
             attrib1: 'Yellow',
             attrib2: 'S',
             discount: '300',
             SKU: '1wgg56',
             inventory: '45'
         }, {
             id: 4,
             attrib1: 'Blue',
             attrib2: 'S',
             discount: '400',
             SKU: '1g3s5s',
             inventory: '23'
         }];

         $scope.changeSelectDeselectAll = function() {
             _.each($scope.itemArray, function(product) {
                 product.selected = $scope.selectDeselectAll;
             });
         };
     });
