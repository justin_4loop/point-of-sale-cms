 /*jslint camelcase:false*/
 'use strict';

 angular.module('pointofsale')
     .controller('purchaseOrderCtrl', function(_, $scope, Pagination) {
         $scope.data = [{
             order_no: '3326',
             order_data: '10/21/2013',
             order_time: '3:29 PM',
             amount: '$321.33'
         }, {
             order_no: '3325',
             order_data: '10/21/2013',
             order_time: '3:20 PM',
             amount: '$234.34'
         }, {
             order_no: '3324',
             order_data: '10/21/2013',
             order_time: '3:03 PM',
             amount: '$724.17'
         }, {
             order_no: '3323',
             order_data: '10/21/2013',
             order_time: '3:00 PM',
             amount: '$23.71'
         }, {
             order_no: '3322',
             order_data: '10/21/2013',
             order_time: '2:49 PM',
             amount: '$8345.23'
         }, {
             order_no: '3321',
             order_data: '10/21/2013',
             order_time: '2:23 PM',
             amount: '$245.12'
         }, {
             order_no: '3320',
             order_data: '10/21/2013',
             order_time: '2:15 PM',
             amount: '$5663.54'
         }];
         $scope.pagination = Pagination.getNew(12);
         $scope.pagination.numPages = Math.ceil($scope.data.length / $scope.pagination.perPage);

         $scope.changeSelectDeselectAll = function() {
             _.each($scope.data, function(product) {
                 product.selected = $scope.selectDeselectAll;
             });
         };
     });
