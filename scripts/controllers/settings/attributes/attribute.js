'use strict';

angular.module('pointofsale')
    .controller('settingsAttribute', function($scope, $window, toastr, $state, $stateParams, $activityIndicator, attributeFactory, Upload, API_URL) {
        console.log("Test console");
        console.log("$stateParams.data", $stateParams.data);

        $scope.stepsModel = [];
        $scope.stepsModel.push('img/default.png');

        // $scope.attributeShow = 'multiAndDrop';
        // $scope.attributeShow = 'multiAndDrop';
        $scope.attrib = {};
        $scope.attrib.fields = {};
        $scope.attrib.attachment = {};
        $scope.attrib.attachment.attchtype = {};
        $scope.attrib.Attvalue = [{
            value: '',
            sku: '',
            value_image: ''
        }];
        $scope.attrib.Attcolor = [{
            name: '',
            color: ''
        }];
        // $scope.attrib. = [{
        //     value_image: ''
        // }];
        $scope.attrib.fields.is_visible = false;
        $scope.attrib.fields.is_required = false;
        $scope.attrib.fields.is_variation = false;
        $scope.attrib.fields.is_custumizable = false;
        $scope.attrib.fields.is_unique = false;


        $scope.attrib.attachment.is_single = false;
        $scope.attrib.attachment.attchtype.attachment_Document = false;
        $scope.attrib.attachment.attchtype.attachment_Image = false;
        $scope.attrib.attachment.attchtype.attachment_Audio = false;
        $scope.attrib.attachment.attchtype.attachment_Video = false;


        attributeFactory.getAllProdType().then(function(data) {
            if (data.statusCode == 200 && data.response.success) {
                $scope.productType = data.response.result;
                /* jshint ignore:start */
                /* jshint ignore:end */

                console.log("$scope.productType", $scope.productType);

            } else {
                toastr.error(data.response.msg, 'Error');
            }

        });

        if ($stateParams.data) {

            attributeFactory.getAttribute($stateParams.data).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    if (data.response && _.isArray(data.response.result)) {

                        console.log("data.response.result[0]", data.response.result[0]);
                        $scope.attrib.fields = data.response.result[0];

                        // $scope.attrib.fields.productType_id = data.response.result[0].ptype_id;

                        $scope.attrib.Attvalue = [{
                            value: data.response.result[0].av_value,
                            sku: data.response.result[0].av_sku,
                            value_image: data.response.result[0].av_image
                        }];

                        $scope.attrib.Attcolor = [{
                            name: data.response.result[0].av_colorname,
                            color: data.response.result[0].av_color
                        }];

                        $scope.attrib.fields.is_visible = data.response.result[0].is_visible;
                        $scope.attrib.fields.is_required = data.response.result[0].is_required;
                        $scope.attrib.fields.is_variation = data.response.result[0].is_variation;
                        $scope.attrib.fields.is_custumizable = data.response.result[0].is_custumizable;
                        $scope.attrib.fields.is_unique = data.response.result[0].is_unique;

                        $scope.attrib.attachment.is_single = data.response.result[0].is_single;
                        $scope.attrib.attachment.attchtype.attachment_Document = data.response.result[0].is_document;
                        $scope.attrib.attachment.attchtype.attachment_Image = data.response.result[0].is_image;
                        $scope.attrib.attachment.attchtype.attachment_Audio = data.response.result[0].is_audio;
                        $scope.attrib.attachment.attchtype.attachment_Video = data.response.result[0].is_video;

                        ($scope.attrib.fields.is_visible ? $scope.attrib.fields.is_visible = true : $scope.attrib.fields.is_visible = false);
                        ($scope.attrib.fields.is_required ? $scope.attrib.fields.is_required = true : $scope.attrib.fields.is_required = false);
                        ($scope.attrib.fields.is_variation ? $scope.attrib.fields.is_variation = true : $scope.attrib.fields.is_variation = 0);
                        ($scope.attrib.fields.is_custumizable ? $scope.attrib.fields.is_custumizable = true : $scope.attrib.fields.is_custumizable = false);
                        ($scope.attrib.fields.is_unique ? $scope.attrib.fields.is_unique = true : $scope.attrib.fields.is_unique = false);
                        // $scope.attrib.is_required = 0;

                        ($scope.attrib.attachment.is_single ? $scope.attrib.attachment.is_single = true : $scope.attrib.attachment.is_single = false);
                        ($scope.attrib.attachment.attchtype.attachment_Document ? $scope.attrib.attachment.attchtype.attachment_Document = true : $scope.attrib.attachment.attchtype.attachment_Document = false);
                        ($scope.attrib.attachment.attchtype.attachment_Image ? $scope.attrib.attachment.attchtype.attachment_Image = true : $scope.attrib.attachment.attchtype.attachment_Image = false);
                        ($scope.attrib.attachment.attchtype.attachment_Audio ? $scope.attrib.attachment.attchtype.attachment_Audio = true : $scope.attrib.attachment.attchtype.attachment_Audio = false);
                        ($scope.attrib.attachment.attchtype.attachment_Video ? $scope.attrib.attachment.attchtype.attachment_Video = true : $scope.attrib.attachment.attchtype.attachment_Video = false);

                        
                        




                        console.log($scope.attrib);
                        $activityIndicator.stopAnimating();
                        toastr.info('Store data loaded', 'Information');
                    }
                }
            });
        }

        $scope.addValue = function() {
            $scope.attrib.Attvalue.push({
                value: '',
                sku: '',
                value_image: ''
            });
            // $scope.attrib.Attvalue.valImage.push({
            //     value_image: ''
            // });
        };

        $scope.addColor = function() {
            $scope.attrib.Attcolor.push({
                name: '',
                color: ''
            });

        };

        $scope.imageUpload = function(element) {
            var reader = new FileReader();
            reader.onload = $scope.imageIsLoaded;
            reader.readAsDataURL(element.files[0]);
        };
        $scope.imageIsLoaded = function(e) {
            console.log("$scope.stepsModel", $scope.stepsModel);
            console.log("e", e);
            $scope.stepsModel.length = 0;
            $scope.$apply(function() {
                $scope.stepsModel.push(e.target.result);
            });
        };
        $scope.cancel = function() {
            // init();
        };
        $scope.removethumb = function() {
            $scope.stepsModel[0] = 'img/default.png';
            // $scope.Categsave.c_image = null;
            // $scope.attrib.Attvalue.value_image: null

        };

        // $scope.saveData = function() {
        // console.log("Save Data ni Dri");
        // $scope.attrib = {};


        $scope.onSubmit = function(valid) {

            console.log("$stateParams.data", $stateParams.data);

            // console.log("L", valid);

            if (valid) {
                console.log("I'm Submitted");

                // saveCategory();

                // console.log("Valid", data);
                ($scope.attrib.fields.is_visible ? $scope.attrib.fields.is_visible = 1 : $scope.attrib.fields.is_visible = 0);
                // ($scope.attrib.is_search ? $scope.attrib.is_search = 1 : $scope.attrib.is_search = 0);
                ($scope.attrib.fields.is_required ? $scope.attrib.fields.is_required = 1 : $scope.attrib.fields.is_required = 0);
                ($scope.attrib.fields.is_variation ? $scope.attrib.fields.is_variation = 1 : $scope.attrib.fields.is_variation = 0);
                ($scope.attrib.fields.is_custumizable ? $scope.attrib.fields.is_custumizable = 1 : $scope.attrib.fields.is_custumizable = 0);
                ($scope.attrib.fields.is_unique ? $scope.attrib.fields.is_unique = 1 : $scope.attrib.fields.is_unique = 0);
                // $scope.attrib.is_required = 0;

                ($scope.attrib.attachment.is_single ? $scope.attrib.attachment.is_single = 1 : $scope.attrib.attachment.is_single = 0);
                ($scope.attrib.attachment.attchtype.attachment_Document ? $scope.attrib.attachment.attchtype.attachment_Document = 1 : $scope.attrib.attachment.attchtype.attachment_Document = 0);
                ($scope.attrib.attachment.attchtype.attachment_Image ? $scope.attrib.attachment.attchtype.attachment_Image = 1 : $scope.attrib.attachment.attchtype.attachment_Image = 0);
                ($scope.attrib.attachment.attchtype.attachment_Audio ? $scope.attrib.attachment.attchtype.attachment_Audio = 1 : $scope.attrib.attachment.attchtype.attachment_Audio = 0);
                ($scope.attrib.attachment.attchtype.attachment_Video ? $scope.attrib.attachment.attchtype.attachment_Video = 1 : $scope.attrib.attachment.attchtype.attachment_Video = 0);


                console.log($scope.attrib);

                // SettingsAttribFactory.saveAttrib($scope.attrib).then(function(data) {
                //     console.log(data);
                //     $window.alert('saved');
                //     if (data.statusCode === 200) {

                //         $state.go('main.settingsAttribute');
                //     }
                // });

                ($scope.attrib.Attvalue.value_image === 'img/default.png' ? $scope.attrib.Attvalue.value_image = null : $scope.attrib.Attvalue.value_image = $scope.attrib.Attvalue.value_image);
                // ($scope.attrib.Attvalue.value_image === 'img/default.png' ? $scope.attrib.Attvalue.value_image = null : $scope.attrib.Attvalue.value_image = $scope.attrib.Attvalue.value_image);

                if (!$stateParams.data) {
                    // console.log("$scope.attrib.Attvalue.value_image", $scope.attrib.Attvalue.value_image);
                    Upload.upload({
                        url: API_URL + '/attributes',
                        fields: $scope.attrib,
                        value_image: $scope.attrib.Attvalue.value_image
                    }).then(function(resp) {
                        console.log($scope.attrib);
                        console.log('Success ');
                        // toastr.success(resp.msg, 'Success');
                        // init();
                    }, function(resp) {
                        console.log('Error status: ' + resp.status);
                    }, function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });
                } else {
                    Upload.upload({
                        url: API_URL + '/attributes/' + $stateParams.data,
                        fields: $scope.attrib,
                        method: 'PUT',
                        value_image: $scope.attrib.Attvalue.valImage
                    }).then(function(resp) {
                        console.log($scope.attrib);
                        console.log('Success ');
                        // init();
                    }, function(resp) {
                        console.log('Error status: ' + resp.status);
                    }, function(evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    });
                }


            } else {

                console.log("Invalid Form");

            }

        };


        // };
    });
