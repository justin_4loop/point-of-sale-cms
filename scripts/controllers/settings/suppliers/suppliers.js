/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('supplierCtrl', function($scope, $state, $stateParams, $activityIndicator, $timeout, supplierFctry, ngDialog, toastr, ngTableParams) {

        $scope.init = function() {
            $scope.suppliers = [];
            $activityIndicator.startAnimating();
            supplierFctry.getAllSuppliers().then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    $scope.suppliers = data.response.result;
                             /* jshint ignore:start */
                    $scope.supplierTable = new ngTableParams({
                        page: 1,
                        count: 3
                    }, {
                        total: $scope.suppliers.length,
                        getData: function($defer, params) {
                            // $scope.data = params.sorting() ? $filter('orderBy')($scope.stores, params.orderBy()) : $scope.stores;
                            $scope.data = $scope.suppliers.slice((params.page() - 1) * params.count(), params.page() * params.count());
                            $defer.resolve($scope.data);
                        }
                    });
                        /* jshint ignore:end */
                    $activityIndicator.stopAnimating();
                    toastr.info('List of suppliers successfully loaded', 'Information');
                } else {
                    toastr.error(data.response.msg, 'Error');
                }

            });
        };

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.suppliers, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };

        $scope.deleteAll = function() {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {

            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.toggleModal = function(id, name, index) {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                $scope.removestore(id);
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.removestore = function(id) {
            supplierFctry.deleteSupplier(id, function(data) {
                console.log('data: ', data);
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.init();
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        };

        $scope.edit = function(data) {
            $state.go('main.settingsAddStore', {
                data: data.store_id
            });
        };
    });
