'use strict';

angular.module('pointofsale')
    .controller('banksCtrl', function($rootScope, $scope, $state, $activityIndicator, $timeout, $filter, toastr, ngDialog, banksFctry, ngTableParams) {

        $scope.selectedDatas = [];

        $scope.init = function() {
            $scope.banks = [];

            $activityIndicator.startAnimating();
            /* jshint ignore:start */
            $scope.bankTable = new ngTableParams({
                page: 1,
                count: 10
            }, {
                total: $scope.banks.length,
                getData: function($defer, params) {
                    banksFctry.getAllBanks().then(function(data) {
                        if (data.statusCode == 200 && data.response.success) {
                            $scope.banks = data.response.result;

                            if ($scope.toSearch) {
                                $scope.banks = $filter('filter')(data.response.result, $scope.toSearch);
                                params.total($scope.banks.length);
                                var responsedata = $scope.banks.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(responsedata);
                            } else {
                                params.total($scope.banks.length);
                                var responsedata = $scope.banks.slice((params.page() - 1) * params.count(), params.page() * params.count());
                                $defer.resolve(responsedata);
                            }
                            $activityIndicator.stopAnimating();
                            toastr.info('List of Banks successfully loaded', 'Information');
                        } else {
                            toastr.error(data.response.msg, 'Error');
                        }
                    });
                }
            });
            /* jshint ignore:end */
        };

        $scope.refresh = function() {
            $scope.bankTable.reload();
        };

        $scope.changeSelectDeselectAll = function() {
            $scope.selectDeselectAll = !$scope.selectDeselectAll;
            _.each($scope.banks, function(product) {
                product.selected = $scope.selectDeselectAll;
                $scope.deleteSomeData = $scope.selectDeselectAll;
                $scope.checkDataChanges(product);
            });
        };

        $scope.checkDataChanges = function(contact) {
            if (contact.selected) {
                if (!_.find($scope.selectedDatas, function(selectedData) {
                        return selectedData.bk_id === contact.bk_id;
                    })) {
                    $scope.selectedDatas.push(contact);
                }
            } else {
                $scope.selectedDatas = _.filter($scope.selectedDatas, function(selectedData) {
                    return selectedData.bk_id !== contact.bk_id;
                });
            }
            $scope.deleteSomeData = ($scope.selectedDatas.length > 0) ? true : false;
        };

        $scope.deleteSelected = function() {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                var dataCountToDelete = 0;
                _.each($scope.selectedDatas, function(contact) {
                    if (contact.selected) {
                        ++dataCountToDelete;

                        banksFctry.deleteBank(contact.bk_id).then(function(data) {
                            if (data.statusCode == 200 && data.response.success) {
                                toastr.success(data.response.msg, 'Success');
                                $state.reload();
                            } else {
                                toastr.error(data.response.msg, 'Error');
                                return;
                            }
                        });
                    }
                });
                $scope.refresh();
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.toggleModal = function(id, index) {
            ngDialog.openConfirm({
                templateUrl: 'templates/template/deleteDialog.html',
                scope: $scope,
                className: 'ngdialog-theme-default'
            }).then(function() {
                $scope.removeBank(id);
            }, function(reason) {
                console.log('Modal promise close: ');
            });
        };

        $scope.removeBank = function(id) {
            banksFctry.deleteBank(id).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    toastr.success(data.response.msg, 'Success');
                    $timeout(function() {
                        $scope.refresh();
                    }, 300);
                } else {
                    toastr.error(data.response.msg, 'Error');
                }
            });
        };

        $scope.edit = function(data) {
            $state.go('main.settingsBank', {
                data: data.bk_id
            });
        };
    });
