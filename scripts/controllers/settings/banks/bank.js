/*jslint camelcase:false*/
'use strict';
angular.module('pointofsale')
    .controller('bankCtrl', function(_, $scope, $window, $state, $stateParams, $timeout, toastr, $activityIndicator, banksFctry) {
        $scope.bank = {};

        if (_.isEmpty($stateParams.data) || _.isUndefined($stateParams.data)) {
            $scope.bank = {};
        } else {
            $activityIndicator.startAnimating();
            banksFctry.getBank($stateParams.data).then(function(data) {
                if (data.statusCode == 200 && data.response.success) {
                    if (data.response && _.isArray(data.response.result)) {
                        $scope.bank = data.response.result[0];
                        $activityIndicator.stopAnimating();
                        toastr.info('Bank data loaded', 'Information');
                    }
                }
            });
        }

        $scope.saveBank = function() {
            if (_.isEmpty($stateParams.data) || _.isUndefined($stateParams.data)) {
                banksFctry.saveBank($scope.bank).then(function(data) {
                    if (data.statusCode === 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.go('main.settingsBanks');
                        }, 300);
                    } else if (data.statusCode === 200 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result,function(row){
                            toastr.error(row.msg, 'Error');
                        });
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                });
            } else {
                banksFctry.updateBank($stateParams.data, $scope.bank).then(function(data) {
                    if (data.statusCode == 200 && data.response.success) {
                        toastr.success(data.response.msg, 'Success');
                        $timeout(function() {
                            $state.go('main.settingsBanks');
                        }, 300);
                    } else if (data.statusCode === 200 && !data.response.success && _.isArray(data.response.result)) {
                        _.each(data.response.result,function(row){
                            toastr.error(row.msg, 'Error');
                        });
                    } else {
                        toastr.error(data.response.msg, 'Error');
                        return;
                    }
                });
            }
        };
    });
