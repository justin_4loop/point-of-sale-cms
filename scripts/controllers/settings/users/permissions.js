 'use strict';

 angular.module('pointofsale')
     .controller('permissionCtrl', function(_, async, $scope, $timeout, $activityIndicator, $stateParams, toastr, permissionFctry, usergroupsFctry) {
         $scope.permissions = [];
         $scope.Groups = [];
         $scope.usergroups = [];
         $scope.isReset = true;
         $scope.isUpdate = true;

         function flattenArray(array, fn) {
             var output = [];
             for (var i = 0; i < array.length; ++i) {
                 var result = fn(array[i]);
                 if (result) {
                     output = output.concat(result);
                 }
             }
             return output;
         }

         $activityIndicator.startAnimating();
         async.waterfall([
             function(callback) {
                 usergroupsFctry.getAllUserGroups().then(function(data) {
                     if (data.statusCode == 200 && data.response.success) {
                         $scope.usergroups = data.response.result;
                         callback(null, $scope.usergroups);
                     } else {
                         callback(null, 'error');
                         $activityIndicator.stopAnimating();
                     }
                 });
             },
             function(resp, callback) {
                 if (_.isUndefined($stateParams.roleID)) {
                     permissionFctry.getAllModulesPermissions().then(function(data) {
                         if (data.statusCode == 200 && data.response.success) {
                             var permissionsArr = data.response.result;
                             if (permissionsArr.length > 0) {

                                 $scope.permissions = permissionsArr;
                                 _.each($scope.permissions, function(product) {
                                     product.DefaultRead = false;
                                     product.DefaultReadDisable = true;

                                     product.DefaultWrite = false;
                                     product.DefaultWriteDisable = true;

                                     product.DefaultDelete = false;
                                     product.DefaultDeleteDisable = true;

                                     product.DefaultPrint = false;
                                     product.DefaultPrintDisable = true;
                                 });

                                 $activityIndicator.stopAnimating();
                                 callback(null, $scope.permissions);
                             }
                         }
                     });
                 } else {
                     var role = JSON.parse($stateParams.roleID);
                     delete role.$$hashKey;
                     console.log('stateParams: ', role);
                     $scope.generateAccessPermission(role);
                 }
             }
         ]);


         $scope.generateAccessPermission = function(usergroupSelected) {
             $scope.usergroupSelected = usergroupSelected;
             $scope.permissions = [];
             $activityIndicator.startAnimating();
             $timeout(function() {
                 if (_.isUndefined($scope.usergroupSelected)) {
                     $scope.clear();
                 } else {
                     permissionFctry.generateAccessPermission(usergroupSelected.role_id).then(function(data) {
                         console.log('data: ', data);
                         if (data.statusCode == 200 && data.response.success) {
                             $scope.isUpdate = false;
                             $scope.isReset = false;
                             var permissionsArr = data.response.result;
                             if (permissionsArr.length > 0) {
                                 $scope.permissions = permissionsArr;
                                 _.each($scope.permissions, function(product) {
                                     product.DefaultReadDisable = false;
                                     product.DefaultWriteDisable = false;
                                     product.DefaultDeleteDisable = false;
                                     product.DefaultPrintDisable = false;
                                 });

                                 $activityIndicator.stopAnimating();
                             } else {
                                 $activityIndicator.stopAnimating();
                             }
                         }
                     });
                 }
             }, 300);
         };

         $scope.clear = function() {
             $scope.isUpdate = true;
             $scope.isReset = true;
             $scope.usergroupSelected = undefined;
             $scope.permissions = [];
             $activityIndicator.startAnimating();
             permissionFctry.getAllModulesPermissions().then(function(data) {
                 if (data.statusCode == 200 && data.response.success) {
                     var permissionsArr = data.response.result;
                     if (permissionsArr.length > 0) {

                         $scope.permissions = permissionsArr;
                         _.each($scope.permissions, function(product) {
                             product.DefaultRead = false;
                             product.DefaultReadDisable = true;

                             product.DefaultWrite = false;
                             product.DefaultWriteDisable = true;

                             product.DefaultDelete = false;
                             product.DefaultDeleteDisable = true;

                             product.DefaultPrint = false;
                             product.DefaultPrintDisable = true;
                         });

                         $activityIndicator.stopAnimating();
                     }
                 }
             });
         };

         $scope.reloadData = function() {
             if (_.isUndefined($scope.usergroupSelected)) {
                 $scope.clear();
             } else {
                 $scope.generateAccessPermission($scope.usergroupSelected);
             }
         };

         $scope.UpdatePermission = function() {
             $scope.newPermission = angular.copy($scope.permissions);
             permissionFctry.setGroupPrivilege($scope.usergroupSelected.role_id, $scope.newPermission).then(function(data) {
                 if (data.statusCode == 200 && data.response.success) {
                     toastr.success(data.response.msg, 'Success');
                     $scope.reloadData();
                 } else {
                     toastr.error(data.response.msg, 'Error');
                 }
             });
         };


         $scope.IntToBoolean = function(intValue) {
             return intValue == 1 ? true : false;
         };

         $scope.BooleanToInt = function(boolean) {
             return boolean === true ? 1 : 0;
         };

         $scope.myFunction = function() {
             angular.element(document).ready(function() {
                 $('#myTable').each(function() {
                     var Column_number_to_Merge = 1;
                     var Previous_TD = null;
                     var i = 1;
                     $("tbody", this).find('tr').each(function() {
                         var Current_td = $(this).find('td:nth-child(' + Column_number_to_Merge + ')');

                         if (Previous_TD == null) {
                             Previous_TD = Current_td;
                             i = 1;
                         } else if ($(Current_td).scope().row.name == $(Previous_TD).scope().row.name) {
                             Current_td.remove();
                             Previous_TD.attr('rowspan', i + 1);
                             i = i + 1;
                         } else {
                             Previous_TD = Current_td;
                             i = 1;
                         }
                     });
                 });
             });
         };
     });
