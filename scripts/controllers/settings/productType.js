/*jslint eqeqeq:false*/

'use strict';
angular.module('pointofsale')
    .controller('ACPTypeCTRL', function($scope, $state, $stateParams, $timeout, acPTypefctry, Upload, $http, API_URL, photoapi,Pagination) {
        $scope.PType = {};
        $scope.prodTypdeData = [];
            /* jshint ignore:start */
        if ($stateParams.data == null && $stateParams.data == undefined) {

            $scope.ptypeSave = {};
            $scope.ptypeSave.ptype_image = 'img/default.png';
        } else {
            $scope.ptypeSave = $stateParams.data;
            console.log($scope.ptypeSave);
            if ($scope.ptypeSave.ptype_image === 'null' || $scope.ptypeSave.ptype_image == undefined) {
                $scope.ptypeSave.ptype_image = 'img/default.png';
            } else {
                $scope.origImage = $stateParams.data.ptype_image;
                $scope.ptypeSave.ptype_image = photoapi + $stateParams.data.ptype_image;

            }
        }
        acPTypefctry.getAllProdType().then(function(data) {
            $scope.PType = data.response;
            console.log(data.response);
            $scope.PType.forEach(function(data) {
                $scope.prodTypdeData.push({
                    description: data.description,
                    label: data.label,
                    name: data.name,
                    ptype_id: data.ptype_id,
                    ptype_image: data.ptype_image

                });
            });

            $scope.pagination = Pagination.getNew(3);
            $scope.pagination.numPages = Math.ceil($scope.prodTypdeData.length / $scope.pagination.perPage);
        });
        /* jshint ignore:end */

        $scope.changeSelectDeselectAll = function() {
            _.each($scope.PType, function(product) {
                product.selected = $scope.selectDeselectAll;
            });
        };

        $scope.removePType = function() {
            console.log($scope.todelete.TDid);
            acPTypefctry.deleteProdType($scope.todelete.TDid, function(data) {
                if (data.statusCode === 200) {
                    $scope.showModal = false;
                    $scope.PType.splice($scope.todelete.TDindex, 1);
                }
            });
        };
        $scope.edit = function(data) {
            $scope.ptypeSave = data;

            $state.go('main.settingsAddProducttype', {
                data: data
            });

        };

        $scope.showModal = false;
        $scope.toggleModal = function(id, name, index) {
            $scope.showModal = !$scope.showModal;
            $scope.todelete = {
                TDid: id,
                TDname: name,
                TDindex: index
            };
        };



        $scope.acsaveProdType = function() {
            if ($scope.ptypeSave.ptype_image === 'img/default.png') {
                $scope.ptypeSave.ptype_image = null;
            } else {
                $scope.ptypeSave.ptype_image = $scope.ptypeSave.ptype_image;
            }



            if ($scope.ptypeSave.ptype_id == null) {
                // acPTypefctry.saveProdType($scope.ptypeSave).then(function(data) {
                //     if (data.statusCode == 200) {
                //         init();
                //         $state.go('main.settingsProducttype')
                //     }
                Upload.upload({
                    url: API_URL + '/productType',
                    fields: $scope.ptypeSave,

                    ptype_image: $scope.ptypeSave.ptype_image
                }).then(function(resp) {
                    $state.go('main.settingsProducttype');
                    console.log($scope.ptypeSave);
                    console.log('Success ');

                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    // console.log('progress: ' + progressPercentage);

                });

            } else {
                console.log($scope.ptypeSave);

                if ($scope.ptypeSave.ptype_image === photoapi + $scope.origImage) {
                    $scope.ptypeSave.ptype_image = $scope.origImage;
                }
                Upload.upload({
                    url: API_URL + '/productType/' + $scope.ptypeSave.ptype_id,
                    fields: $scope.ptypeSave,
                    method: 'PUT',
                    ptype_image: $scope.ptypeSave.ptype_image
                }).then(function(resp) {
                    $state.go('main.settingsProducttype');
                    console.log($scope.ptypeSave);
                    console.log('Success ');
                }, function(resp) {
                    console.log('Error status: ' + resp.status);
                }, function(evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    // console.log('progress: ' + progressPercentage);

                });
            }
        };

        $scope.removethumb = function() {

            $scope.ptypeSave.ptype_image = 'img/default.png';
        };
        $scope.close = function() {
            $scope.showModal = false;
        };

        $scope.uploadPic = function(file) {
            file.upload = Upload.upload({
                url: 'https://angular-file-upload-cors-srv.appspot.com/upload',
                data: {
                    file: file,
                    username: $scope.username
                },
            });
            file.upload.then(function(response) {
                $timeout(function() {
                    file.result = response.data;
                });
            }, function(response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
            }, function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            });
        };
    });
