'use strict';
angular.module('pointofsale')
    .config(function($stateProvider, $urlRouterProvider, $locationProvider, $validationProvider) {
        $stateProvider
            .state('auth', {
                url: '/auth',
                abstract: true,
                templateUrl: 'templates/auth/auth.html'
            })
            .state('auth.login', {
                url: '/login',
                views: {
                    "authContent": {
                        templateUrl: 'templates/auth/login.html',
                        controller: 'AuthCtrl'
                    }
                }
            })
            .state('main', {
                url: '/main',
                abstract: true,
                templateUrl: 'templates/mainPage.html',
                controller: 'mainPageCtrl'
            })
            .state('main.home', {
                url: '/home',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/home.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Dashboard'
                },
                data: {
                    module: 'Dashboard',
                    tab: 0
                }
            })
            .state('main.purchaseorder', {
                url: '/purchaseorder',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/purchaseorder/po.html',
                        controller: 'purchaseOrderCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Purchase Orders',
                    parent: 'main.home'
                },
                data: {
                    module: 'Purchase Orders',
                    tab: 2
                }
            })
            .state('main.campaign', {
                url: '/campaign',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/campaignloyalty/c_and_l.html'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Campaign and Loyalty',
                    parent: 'main.home'
                },
                data: {
                    module: 'Campaign and Loyalty',
                    tab: 3
                }
            })
            .state('main.financials', {
                url: '/financials',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/financials/financial.html',
                        controller: 'financialCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Financials',
                    parent: 'main.home'
                },
                data: {
                    module: 'Financials',
                    tab: 4
                }
            })
            .state('main.reports', {
                url: '/reports',
                views: {
                    "mainContent": {
                        templateUrl: 'templates/reports/main.html',
                        controller: 'mainReportCtrl'
                    }
                },
                ncyBreadcrumb: {
                    label: 'Reports',
                    parent: 'main.home'
                },
                data: {
                    module: 'Reports',
                    tab: 5
                }
            });
        // catch to otherwise if wrong url
        // $urlRouterProvider.otherwise('/main/addproduct');
        $urlRouterProvider.otherwise('/auth/login');
    });
